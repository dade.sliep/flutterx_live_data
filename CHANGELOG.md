## 1.1.0

* Initial stable release

## 1.0.9-dev

* Fixes.

## 1.0.8-dev

* Fixes.

## 1.0.7-dev

* Improve Widgets integration.

## 1.0.6-dev

* Improve postInvalidate integration.

## 1.0.5-dev

* Add postInvalidate function.

## 1.0.4-dev

* Fixes.

## 1.0.3-dev

* Add initialization management.

## 1.0.2-dev

* Update dependencies.

## 1.0.1-dev

* Added example project.

## 1.0.0-dev

* Initial release.
