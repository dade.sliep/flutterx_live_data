import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';

void main() {
  group('Test LiveData', () {
    testWidgets('Test Value', testLiveDataValue);
    testWidgets('Test Active', testLiveDataActive);
    testWidgets('Test Thread', testLiveDataThread);
    testWidgets('Test Map', testLiveDataMap);
  });
}

Future<void> testLiveDataValue(WidgetTester tester) async {
  final data = MutableLiveData(initialValue: 0);
  await tester.pumpWidget(MaterialApp(builder: (context, _) {
    expect(data.value, equals(0)); // Still equal to initial value
    return LiveDataBuilder(data: data, builder: (context, value) => Text(value.toString()));
  }));

  // Displayed value is 0
  expect(find.text('0'), findsOneWidget);
  expect(find.text('1'), findsNothing);

  // Increment value and await frame
  data.value++;
  await tester.pump();

  // Displayed value is 1
  expect(find.text('0'), findsNothing);
  expect(find.text('1'), findsOneWidget);
}

Future<void> testLiveDataActive(WidgetTester tester) async {
  final subscribe = MutableLiveData(initialValue: true);
  final data = MutableLiveData(initialValue: 0);
  // No observer should be found: liveData is inactive
  expect(data.isActive, equals(false));
  await tester.pumpWidget(MaterialApp(builder: (context, _) {
    // During build still no observer should be found
    expect(data.isActive, equals(false));
    return LiveDataBuilder<bool>(
        data: subscribe,
        builder: (context, value) => value
            ? LiveDataBuilder<int>(data: data, builder: (context, value) => Text(value.toString()))
            : Container());
  }));
  // One observer should be found: liveData is active
  expect(data.isActive, equals(true));
  // Force observer widget to be removed from tree and await frame
  subscribe.value = false;
  await tester.pump();
  // Widget should have been removed
  expect(find.text('0'), findsNothing);
  // No observer should be found: liveData is inactive
  expect(data.isActive, equals(false));
}

Future<void> testLiveDataThread(WidgetTester tester) async {
  final data = MutableLiveData(initialValue: 0);
  await tester.pumpWidget(MaterialApp(builder: (context, _) {
    // Setting value during building
    data.value = 17;
    // Value has been updated
    expect(data.value, equals(17));
    return LiveDataBuilder(data: data, builder: (context, value) => Text(value.toString()));
  }));
  expect(data.value, equals(17));
}

Future<void> testLiveDataMap(WidgetTester tester) async {
  final src = MutableLiveData(initialValue: 0);
  final data = MediatorLiveData<String>()..addTransformSource(src, (value) => 'number $value');
  // Src value is initialized
  expect(src.value, 0);
  // Updating source value
  src.value++;
  await tester.pumpWidget(MaterialApp(builder: (context, _) {
    // Changing value during build
    src.value = 666;
    expect(src.value, 666);
    return LiveDataBuilder(data: data, builder: (context, value) => Text(value.toString()));
  }));
  // After build value has been dispatched
  expect(data.value, 'number 666');
  src.value = 777;
  // Value has been updated since computed value has active observers now
  expect(data.value, 'number 777');
  await tester.pumpWidget(MaterialApp(
      builder: (context, _) => MediatorLiveDataBuilder<int, String>(
          data: src, transform: (src) => 'result: $src', builder: (context, value) => Text(value.toString()))));
  expect(find.text('result: 777'), findsOneWidget);
}
