part of '../flutterx_live_data.dart';

/// Dispatcher for [LiveData] value
/// Dispatch submitted action and provide status information
///
/// Available dispatchers are:
/// [immediate] which executes the action synchronously
/// [postFrame] which executes the action only when Widgets tree is not currently building
///
/// To execute an action just call [post]
abstract class Dispatcher {
  /// Executes action immediately. isDispatching callback will never be called
  static const Dispatcher immediate = _SynchronousDispatcher._();

  /// Executes action when Flutter engine is not building the Widgets tree.
  /// If [SchedulerBinding.schedulerPhase] is [SchedulerPhase.idle] the action
  /// is executed immediately without calling isDispatching callback.
  /// Else the action will be executed asynchronously inside [SchedulerBinding.addPostFrameCallback] and
  /// isDispatching callback will be called to notify whether the value is being dispatched or not
  static const Dispatcher postFrame = _PostFrameDispatcher._();

  const Dispatcher();

  /// Executes the given [action].
  /// In case the action is executed synchronously to this method call [isDispatching] callback is never called,
  /// otherwise it will be called immediately with true and then, after [action] has been executed with false
  void post(VoidCallback action, [ValueChanged<bool>? isDispatching]);
}

class _SynchronousDispatcher extends Dispatcher {
  const _SynchronousDispatcher._() : super();

  @override
  void post(VoidCallback action, [ValueChanged<bool>? isDispatching]) => action();
}

class _PostFrameDispatcher extends Dispatcher {
  const _PostFrameDispatcher._() : super();

  @override
  void post(VoidCallback action, [ValueChanged<bool>? isDispatching]) {
    final binding = WidgetsBinding.instance!;
    if (binding.schedulerPhase == SchedulerPhase.idle) return action();
    isDispatching?.call(true);
    binding.addPostFrameCallback((_) {
      action();
      isDispatching?.call(false);
    });
  }
}
