part of '../flutterx_live_data.dart';

/// Equals to [LiveDataBuilder] but allows to have the [builder] type different from sources [data1] and [data2] type by
/// applying a [transform] to a [MediatorLiveData]
///
/// [S1] and [S2] The types of data held by [data1] and [data2] sources
/// [T] The transformed value type
class CombinedLiveDataBuilder<S1, S2, T> extends StatelessWidget {
  /// LiveData source observed by this widget
  final LiveData<S1> data1;

  /// LiveData source observed by this widget
  final LiveData<S2> data2;

  /// Transform applied to sources
  final T Function(S1, S2) transform;

  /// [LiveWidgetBuilder] of this widget content
  final LiveWidgetBuilder<T> builder;

  /// Placeholder to show instead of [builder] when [LiveData.initialized] is false. Defaults to [SizedBox.shrink]
  final Widget? placeholder;

  /// Create a [CombinedLiveDataBuilder] for [data1] and [data2]
  const CombinedLiveDataBuilder({
    Key? key,
    required this.data1,
    required this.data2,
    required this.transform,
    required this.builder,
    this.placeholder,
  }) : super(key: key);

  /// [CombinedLiveDataBuilder] are controlled by [CombinedLiveDataBuilderElement]
  @override
  CombinedLiveDataBuilderElement<S1, S2, T> createElement() => CombinedLiveDataBuilderElement<S1, S2, T>(this);

  /// Delegates to [liveBuild]
  @override
  Widget build(BuildContext context) => liveBuild<T>(context,
      data: (context as CombinedLiveDataBuilderElement<S1, S2, T>).mediator,
      builder: builder,
      placeholder: placeholder);
}

/// Element of [CombinedLiveDataBuilder]
class CombinedLiveDataBuilderElement<S1, S2, T> extends StatelessElement {
  final MediatorLiveData<T> mediator = MediatorLiveData<T>();

  @override
  CombinedLiveDataBuilder<S1, S2, T> get widget => super.widget as CombinedLiveDataBuilder<S1, S2, T>;

  CombinedLiveDataBuilderElement(CombinedLiveDataBuilder<S1, S2, T> widget) : super(widget);

  @override
  void mount(Element? parent, dynamic newSlot) {
    super.mount(parent, newSlot);
    mediator.observeForever(onChanged);
    link(widget.data1, widget.data2);
  }

  @override
  void update(covariant StatelessWidget newWidget) {
    final oldData1 = widget.data1;
    final oldData2 = widget.data2;
    super.update(newWidget);
    if (!identical(oldData1, widget.data1) || !identical(oldData2, widget.data2)) {
      unlink(oldData1, oldData2);
      link(widget.data1, widget.data2);
    }
  }

  @override
  void unmount() {
    mediator.removeObserver(onChanged);
    unlink(widget.data1, widget.data2);
    super.unmount();
  }

  /// Bind this element to [data1] and [data2]
  void link(LiveData<S1> data1, LiveData<S2> data2) => mediator.addTransformSourcePair(data1, data2, widget.transform);

  /// Unbind this element from [data1] and [data2]
  void unlink(LiveData<S1> data1, LiveData<S2> data2) => mediator..removeSource(data1)..removeSource(data2);

  /// Handle [mediator] updates
  void onChanged(T value) => markNeedsBuild();
}
