part of '../flutterx_live_data.dart';

/// Equals to [LiveDataBuilder] but allows to have the [builder] type different from source [data] type by
/// applying a [transform] to a [MediatorLiveData]
///
/// [S] The type of data held by [data] source
/// [T] The transformed value type
class MediatorLiveDataBuilder<S, T> extends StatelessWidget {
  /// LiveData source observed by this widget
  final LiveData<S> data;

  /// Transform applied to source
  final T Function(S) transform;

  /// [LiveWidgetBuilder] of this widget content
  final LiveWidgetBuilder<T> builder;

  /// Placeholder to show instead of [builder] when [LiveData.initialized] is false. Defaults to [SizedBox.shrink]
  final Widget? placeholder;

  /// Create a [MediatorLiveDataBuilder] for [data]
  const MediatorLiveDataBuilder({
    Key? key,
    required this.data,
    required this.transform,
    required this.builder,
    this.placeholder,
  }) : super(key: key);

  /// [MediatorLiveDataBuilder] are controlled by [MediatorLiveDataBuilderElement]
  @override
  MediatorLiveDataBuilderElement<S, T> createElement() => MediatorLiveDataBuilderElement<S, T>(this);

  /// Delegates to [liveBuild]
  @override
  Widget build(BuildContext context) => liveBuild<T>(context,
      data: (context as MediatorLiveDataBuilderElement<S, T>).mediator, builder: builder, placeholder: placeholder);
}

/// Element of [MediatorLiveDataBuilder]
class MediatorLiveDataBuilderElement<S, T> extends StatelessElement {
  final MediatorLiveData<T> mediator = MediatorLiveData<T>();

  @override
  MediatorLiveDataBuilder<S, T> get widget => super.widget as MediatorLiveDataBuilder<S, T>;

  MediatorLiveDataBuilderElement(MediatorLiveDataBuilder<S, T> widget) : super(widget);

  @override
  void mount(Element? parent, dynamic newSlot) {
    super.mount(parent, newSlot);
    mediator.observeForever(onChanged);
    link(widget.data);
  }

  @override
  void update(covariant StatelessWidget newWidget) {
    final oldData = widget.data;
    super.update(newWidget);
    if (!identical(oldData, widget.data)) {
      unlink(oldData);
      link(widget.data);
    }
  }

  @override
  void unmount() {
    mediator.removeObserver(onChanged);
    unlink(widget.data);
    super.unmount();
  }

  /// Bind this element to [data]
  void link(LiveData<S> data) => mediator.addTransformSource(data, widget.transform);

  /// Unbind this element from [data]
  void unlink(LiveData<S> data) => mediator.removeSource(data);

  /// Handle [mediator] updates
  void onChanged(T value) => markNeedsBuild();
}
