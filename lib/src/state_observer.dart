part of '../flutterx_live_data.dart';

/// Link [LiveData] observers to the lifecycle of your [State]
///
/// Implement this mixin on your widget's [State] to add [Observer]s to a [LiveData] within the state lifecycle
/// Every LiveData can be observed inside [registerObservers] method that you can override
/// The observer must be added through [observe] method and will be removed automatically on state [dispose]
mixin StateObserver<T extends StatefulWidget> on State<T> {
  final List<VoidCallback> _disposers = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) => registerObservers(context));
  }

  @override
  void dispose() {
    super.dispose();
    for (final dispose in _disposers) dispose();
  }

  /// Override this method to observe you LiveData(s)
  ///
  /// This is called only once, after the first frame of this widget is scheduled
  void registerObservers(BuildContext context);

  /// Observe a [LiveData] for all the lifecycle of this state
  void observe<V>(LiveData<V> data, Observer<V> onChanged) {
    data.observeForever(onChanged);
    _disposers.add(() => data.removeObserver(onChanged));
  }
}
