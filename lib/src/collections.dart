part of '../flutterx_live_data.dart';

/// [LiveData] which represent a collection
///
/// Every subclass of [LiveCollection] must implement [C] and comes a proxy between the observers and it's source
/// Every single change to the collection will [invalidate] this LiveData and notify it's observers
///
/// The source is passed to the constructor and then is not accessible directly from this class (there is no
/// source accessor). Anyway the value received by the [Observer] is the source instance and not this LiveData.
/// This means that if you want to update the collection from the observer you have to reference this instance
/// instead of using [Observer] received value
///
/// To update the entire collection you can call [setTo] which has the function to clear the source and copy
/// the new value into it, anyway the source itself can never be reassigned
///
/// [C] The type of collection held by this instance
abstract class LiveCollection<C> extends LiveData<C> {
  /// Creates a [LiveCollection] from given [source]
  /// [dispatcher] by default is [Dispatcher.postFrame]
  LiveCollection(C source, {Dispatcher dispatcher = Dispatcher.postFrame}) : super(dispatcher: dispatcher) {
    super.setData(source);
  }

  /// Clear the source and deep copy [value] into it, then [invalidate]
  /// Consider using [postSetTo]
  void setTo(C value);

  /// Post [setTo] call to [dispatcher]
  void postSetTo(C value) => super.postData(value);

  /// Cannot set new source instance, only edit the existing one
  @override
  @Deprecated('Use setTo')
  void setData(C value) => setTo(value);

  /// Cannot set new source instance, only edit the existing one
  @override
  @Deprecated('Use postSetTo')
  void postData(C value) => postSetTo(value);
}

/// [LiveCollection] which implements [List]
class LiveList<E> extends LiveCollection<List<E>> implements List<E> {
  /// check [LiveCollection] constructor
  LiveList({List<E>? source, Dispatcher dispatcher = Dispatcher.postFrame})
      : super(source ?? <E>[], dispatcher: dispatcher);

  @override
  void setTo(List<E> value) {
    _data.clear();
    _data.addAll(value);
    invalidate();
  }

  @override
  bool any(bool Function(E) test) => _data.any(test);

  @override
  List<T> cast<T>() => _data.cast<T>();

  @override
  bool contains(Object? element) => _data.contains(element);

  @override
  E elementAt(int index) => _data.elementAt(index);

  @override
  bool every(bool Function(E) test) => _data.every(test);

  @override
  Iterable<T> expand<T>(Iterable<T> Function(E) f) => _data.expand(f);

  @override
  E get first => _data.first;

  @override
  E firstWhere(bool Function(E) test, {E Function()? orElse}) => _data.firstWhere(test, orElse: orElse);

  @override
  T fold<T>(T initialValue, T Function(T previousValue, E element) combine) => _data.fold(initialValue, combine);

  @override
  Iterable<E> followedBy(Iterable<E> other) => _data.followedBy(other);

  @override
  void forEach(void Function(E) f) => _data.forEach(f);

  @override
  bool get isEmpty => _data.isEmpty;

  @override
  bool get isNotEmpty => _data.isNotEmpty;

  @override
  Iterator<E> get iterator => _data.iterator;

  @override
  String join([String separator = '']) => _data.join(separator);

  @override
  E get last => _data.last;

  @override
  E lastWhere(bool Function(E) test, {E Function()? orElse}) => _data.lastWhere(test, orElse: orElse);

  @override
  int get length => _data.length;

  @override
  Iterable<T> map<T>(T Function(E) f) => _data.map(f);

  @override
  E reduce(E Function(E value, E element) combine) => _data.reduce(combine);

  @override
  E get single => _data.single;

  @override
  E singleWhere(bool Function(E) test, {E Function()? orElse}) => _data.singleWhere(test, orElse: orElse);

  @override
  Iterable<E> skip(int n) => _data.skip(n);

  @override
  Iterable<E> skipWhile(bool Function(E) test) => _data.skipWhile(test);

  @override
  Iterable<E> take(int n) => _data.take(n);

  @override
  Iterable<E> takeWhile(bool Function(E) test) => _data.takeWhile(test);

  @override
  List<E> toList({bool growable = true}) => _data.toList(growable: growable);

  @override
  Set<E> toSet() => _data.toSet();

  @override
  Iterable<E> where(bool Function(E) test) => _data.where(test);

  @override
  Iterable<T> whereType<T>() => _data.whereType<T>();

  @override
  String toString() => _data.toString();

  @override
  E operator [](int index) => _data[index];

  @override
  void operator []=(int index, E value) {
    if (identical(_data[index], value)) return;
    _data[index] = value;
    postInvalidate();
  }

  @override
  List<E> operator +(List<E> other) => _data + other;

  @override
  void add(E value) {
    _data.add(value);
    postInvalidate();
  }

  @override
  void addAll(Iterable<E> iterable) {
    if (iterable.isEmpty) return;
    _data.addAll(iterable);
    postInvalidate();
  }

  @override
  Map<int, E> asMap() => _data.asMap();

  @override
  void clear() {
    if (_data.isEmpty) return;
    _data.clear();
    postInvalidate();
  }

  @override
  void fillRange(int start, int end, [E? fillValue]) {
    if (start == end) return;
    _data.fillRange(start, end, fillValue);
    postInvalidate();
  }

  @override
  set first(E value) {
    if (identical(_data.first, value)) return;
    _data.first = value;
    postInvalidate();
  }

  @override
  Iterable<E> getRange(int start, int end) => _data.getRange(start, end);

  @override
  int indexOf(E element, [int start = 0]) => _data.indexOf(element, start);

  @override
  int indexWhere(bool Function(E) test, [int start = 0]) => _data.indexWhere(test, start);

  @override
  void insert(int index, E element) {
    _data.insert(index, element);
    postInvalidate();
  }

  @override
  void insertAll(int index, Iterable<E> iterable) {
    if (iterable.isEmpty) return;
    _data.insertAll(index, iterable);
    postInvalidate();
  }

  @override
  set last(E value) {
    if (identical(_data.last, value)) return;
    _data.last = value;
    postInvalidate();
  }

  @override
  int lastIndexOf(E element, [int? start]) => _data.lastIndexOf(element, start);

  @override
  int lastIndexWhere(bool Function(E) test, [int? start]) => _data.lastIndexWhere(test, start);

  @override
  set length(int newLength) {
    if (_data.length == newLength) return;
    _data.length = newLength;
    postInvalidate();
  }

  @override
  bool remove(Object? value) {
    final result = _data.remove(value);
    if (result) postInvalidate();
    return result;
  }

  @override
  E removeAt(int index) {
    final result = _data.removeAt(index);
    postInvalidate();
    return result;
  }

  @override
  E removeLast() {
    final result = _data.removeLast();
    postInvalidate();
    return result;
  }

  @override
  void removeRange(int start, int end) {
    if (start == end) return;
    _data.removeRange(start, end);
    postInvalidate();
  }

  @override
  void removeWhere(bool Function(E) test) {
    final length = _data.length;
    _data.removeWhere(test);
    if (length != _data.length) postInvalidate();
  }

  @override
  void replaceRange(int start, int end, Iterable<E> iterable) {
    if (start == end) return;
    _data.replaceRange(start, end, iterable);
    postInvalidate();
  }

  @override
  void retainWhere(bool Function(E) test) {
    final length = _data.length;
    _data.retainWhere(test);
    if (length != _data.length) postInvalidate();
  }

  @override
  Iterable<E> get reversed => _data.reversed;

  @override
  void setAll(int index, Iterable<E> iterable) {
    if (index == _data.length || iterable.isEmpty) return;
    _data.setAll(index, iterable);
    postInvalidate();
  }

  @override
  void setRange(int start, int end, Iterable<E> iterable, [int skipCount = 0]) {
    if (start == end || iterable.isEmpty) return;
    _data.setRange(start, end, iterable, skipCount);
    postInvalidate();
  }

  @override
  void shuffle([Random? random]) {
    _data.shuffle(random);
    postInvalidate();
  }

  @override
  void sort([int Function(E, E)? compare]) {
    _data.sort(compare);
    postInvalidate();
  }

  @override
  List<E> sublist(int start, [int? end]) => _data.sublist(start, end);
}

/// MediatorLiveList is the implementation of [MediatorMixin] for [LiveList]
class MediatorLiveList<E> extends LiveList<E> with MediatorMixin<List<E>> {
  /// check [LiveList] constructor
  MediatorLiveList({List<E>? source, Dispatcher dispatcher = Dispatcher.immediate})
      : super(source: source, dispatcher: dispatcher);
}

/// HistoryLiveList is the implementation of [HistoryMixin] for [LiveList]
class HistoryLiveList<E> extends LiveList<E> with HistoryMixin<List<E>> {
  /// check [LiveList] constructor
  HistoryLiveList({List<E>? source, Dispatcher dispatcher = Dispatcher.postFrame})
      : super(source: source, dispatcher: dispatcher);
}

/// [LiveCollection] which implements [Map]
class LiveMap<K, V> extends LiveCollection<Map<K, V>> implements Map<K, V> {
  /// check [LiveCollection] constructor
  LiveMap({Map<K, V>? source, Dispatcher dispatcher = Dispatcher.postFrame})
      : super(source ?? <K, V>{}, dispatcher: dispatcher);

  @override
  void setTo(Map<K, V> value) {
    _data.clear();
    _data.addAll(value);
    invalidate();
  }

  @override
  V? operator [](Object? key) => _data[key];

  @override
  void operator []=(K key, V value) {
    if (identical(_data[key], value)) return;
    _data[key] = value;
    postInvalidate();
  }

  @override
  void addAll(Map<K, V> other) {
    if (other.isEmpty) return;
    _data.addAll(other);
    postInvalidate();
  }

  @override
  void addEntries(Iterable<MapEntry<K, V>> entries) {
    if (entries.isEmpty) return;
    _data.addEntries(entries);
    postInvalidate();
  }

  @override
  void clear() {
    if (_data.isEmpty) return;
    _data.clear();
    postInvalidate();
  }

  @override
  Map<K2, V2> cast<K2, V2>() => _data.cast<K2, V2>();

  @override
  bool containsKey(Object? key) => _data.containsKey(key);

  @override
  bool containsValue(Object? value) => _data.containsValue(value);

  @override
  Iterable<MapEntry<K, V>> get entries => _data.entries;

  @override
  void forEach(void Function(K, V) f) => _data.forEach(f);

  @override
  bool get isEmpty => _data.isEmpty;

  @override
  bool get isNotEmpty => _data.isNotEmpty;

  @override
  Iterable<K> get keys => _data.keys;

  @override
  int get length => _data.length;

  @override
  Map<K2, V2> map<K2, V2>(MapEntry<K2, V2> Function(K, V) transform) => _data.map(transform);

  @override
  V putIfAbsent(K key, V Function() ifAbsent) {
    var updated = false;
    final result = _data.putIfAbsent(key, () {
      updated = true;
      return ifAbsent();
    });
    if (updated) postInvalidate();
    return result;
  }

  @override
  V? remove(Object? key) {
    final length = _data.length;
    final result = _data.remove(key);
    if (length != _data.length) postInvalidate();
    return result;
  }

  @override
  void removeWhere(bool Function(K, V) test) {
    final length = _data.length;
    _data.removeWhere(test);
    if (length != _data.length) postInvalidate();
  }

  @override
  Iterable<V> get values => _data.values;

  @override
  String toString() => _data.toString();

  @override
  V update(K key, V Function(V) update, {V Function()? ifAbsent}) {
    var updated = false;
    final result = _data.update(key, (value) {
      final newValue = update(value);
      updated = !identical(value, newValue);
      return newValue;
    },
        ifAbsent: ifAbsent == null
            ? null
            : () {
                updated = true;
                return ifAbsent();
              });
    if (updated) postInvalidate();
    return result;
  }

  @override
  void updateAll(V Function(K, V) update) {
    _data.updateAll(update);
    postInvalidate();
  }
}

/// MediatorLiveMap is the implementation of [MediatorMixin] for [LiveMap]
class MediatorLiveMap<K, V> extends LiveMap<K, V> with MediatorMixin<Map<K, V>> {
  /// check [LiveMap] constructor
  MediatorLiveMap({Map<K, V>? source, Dispatcher dispatcher = Dispatcher.immediate})
      : super(source: source, dispatcher: dispatcher);
}

/// HistoryLiveMap is the implementation of [HistoryMixin] for [LiveMap]
class HistoryLiveMap<K, V> extends LiveMap<K, V> with HistoryMixin<Map<K, V>> {
  /// check [LiveMap] constructor
  HistoryLiveMap({Map<K, V>? source, Dispatcher dispatcher = Dispatcher.postFrame})
      : super(source: source, dispatcher: dispatcher);
}

/// [LiveCollection] which implements [Set]
class LiveSet<E> extends LiveCollection<Set<E>> implements Set<E> {
  /// check [LiveCollection] constructor
  LiveSet({Set<E>? source, Dispatcher dispatcher = Dispatcher.postFrame})
      : super(source ?? <E>{}, dispatcher: dispatcher);

  @override
  void setTo(Set<E> value) {
    _data.clear();
    _data.addAll(value);
    invalidate();
  }

  @override
  bool any(bool Function(E) test) => _data.any(test);

  @override
  Set<T> cast<T>() => _data.cast<T>();

  @override
  bool contains(Object? element) => _data.contains(element);

  @override
  E elementAt(int index) => _data.elementAt(index);

  @override
  bool every(bool Function(E) test) => _data.every(test);

  @override
  Iterable<T> expand<T>(Iterable<T> Function(E) f) => _data.expand(f);

  @override
  E get first => _data.first;

  @override
  E firstWhere(bool Function(E) test, {E Function()? orElse}) => _data.firstWhere(test, orElse: orElse);

  @override
  T fold<T>(T initialValue, T Function(T previousValue, E element) combine) => _data.fold(initialValue, combine);

  @override
  Iterable<E> followedBy(Iterable<E> other) => _data.followedBy(other);

  @override
  void forEach(void Function(E) f) => _data.forEach(f);

  @override
  bool get isEmpty => _data.isEmpty;

  @override
  bool get isNotEmpty => _data.isNotEmpty;

  @override
  Iterator<E> get iterator => _data.iterator;

  @override
  String join([String separator = '']) => _data.join(separator);

  @override
  E get last => _data.last;

  @override
  E lastWhere(bool Function(E) test, {E Function()? orElse}) => _data.lastWhere(test, orElse: orElse);

  @override
  int get length => _data.length;

  @override
  Iterable<T> map<T>(T Function(E) f) => _data.map(f);

  @override
  E reduce(E Function(E value, E element) combine) => _data.reduce(combine);

  @override
  E get single => _data.single;

  @override
  E singleWhere(bool Function(E) test, {E Function()? orElse}) => _data.singleWhere(test, orElse: orElse);

  @override
  Iterable<E> skip(int n) => _data.skip(n);

  @override
  Iterable<E> skipWhile(bool Function(E) test) => _data.skipWhile(test);

  @override
  Iterable<E> take(int n) => _data.take(n);

  @override
  Iterable<E> takeWhile(bool Function(E) test) => _data.takeWhile(test);

  @override
  List<E> toList({bool growable = true}) => _data.toList(growable: growable);

  @override
  Set<E> toSet() => _data.toSet();

  @override
  Iterable<E> where(bool Function(E) test) => _data.where(test);

  @override
  Iterable<T> whereType<T>() => _data.whereType<T>();

  @override
  String toString() => _data.toString();

  @override
  bool add(E value) {
    final result = _data.add(value);
    if (result) postInvalidate();
    return result;
  }

  @override
  void addAll(Iterable<E> iterable) {
    if (iterable.isEmpty) return;
    final length = _data.length;
    _data.addAll(iterable);
    if (length != _data.length) postInvalidate();
  }

  @override
  void clear() {
    if (_data.isEmpty) return;
    _data.clear();
    postInvalidate();
  }

  @override
  bool containsAll(Iterable<Object?> other) => _data.containsAll(other);

  @override
  Set<E> difference(Set<Object?> other) => _data.difference(other);

  @override
  Set<E> intersection(Set<Object?> other) => _data.intersection(other);

  @override
  E? lookup(Object? element) => _data.lookup(element);

  @override
  bool remove(Object? value) {
    final result = _data.remove(value);
    if (result) postInvalidate();
    return result;
  }

  @override
  void removeAll(Iterable<Object?> elements) {
    final length = _data.length;
    _data.removeAll(elements);
    if (length != _data.length) postInvalidate();
  }

  @override
  void removeWhere(bool Function(E) test) {
    final length = _data.length;
    _data.removeWhere(test);
    if (length != _data.length) postInvalidate();
  }

  @override
  void retainAll(Iterable<Object?> elements) {
    final length = _data.length;
    _data.retainAll(elements);
    if (length != _data.length) postInvalidate();
  }

  @override
  void retainWhere(bool Function(E) test) {
    final length = _data.length;
    _data.retainWhere(test);
    if (length != _data.length) postInvalidate();
  }

  @override
  Set<E> union(Set<E> other) => _data.union(other);
}

/// MediatorLiveSet is the implementation of [MediatorMixin] for [LiveSet]
class MediatorLiveSet<E> extends LiveSet<E> with MediatorMixin<Set<E>> {
  /// check [LiveSet] constructor
  MediatorLiveSet({Set<E>? source, Dispatcher dispatcher = Dispatcher.immediate})
      : super(source: source, dispatcher: dispatcher);
}

/// HistoryLiveSet is the implementation of [HistoryMixin] for [LiveSet]
class HistoryLiveSet<E> extends LiveSet<E> with HistoryMixin<Set<E>> {
  /// check [LiveSet] constructor
  HistoryLiveSet({Set<E>? source, Dispatcher dispatcher = Dispatcher.postFrame})
      : super(source: source, dispatcher: dispatcher);
}
