part of '../flutterx_live_data.dart';

/// [LiveData] mixin which may observe other [LiveData] objects and react on [Observer] events from them.
///
/// This class correctly propagates its active/inactive states down to source [LiveData] objects.
///
/// Consider the following scenario: we have 2 instances of [LiveData], let's name them liveData1 and liveData2,
/// and we want to merge their emissions in one object: liveDataMerger.
/// Then, liveData1 and liveData2 will become sources for the [MediatorLiveData] liveDataMerger and every time
/// [Observer] callback is called for either of them, we set a new value in liveDataMerger.
/// ```dart
/// final liveData1 = MutableLiveData<int>(initialValue: 15);
/// final liveData2 = MutableLiveData<int>(initialValue: 18);
///
/// final liveDataMerger = MediatorLiveData<int>();
/// liveDataMerger.addTransformSourcePair<int, int>(liveData1, liveData2, (p0, p1) => p0 + p1);
/// ```
///
/// Let's consider that we only want 10 values emitted by liveData1, to be merged in the liveDataMerger.
/// Then, after 10 values, we can stop listening to liveData1 and remove it as a source.
/// ```dart
/// liveDataMerger.addSource<int>(liveData1, (value) {
///   var count = 1;
///   count++;
///   liveDataMerger.value = value;
///   if (count > 10) liveDataMerger.removeSource(liveData1);
/// });
/// ```
///
/// [T] The type of data held by this instance
mixin MediatorMixin<Y> on LiveData<Y> {
  final Map<LiveData, _Source> _sources = {};

  /// Plug sources on active
  @override
  void onActive() {
    super.onActive();
    for (final source in _sources.values) source.plug();
  }

  /// Unplug sources on inactive
  @override
  void onInactive() {
    for (final source in _sources.values) source.unplug();
    super.onInactive();
  }

  /// Add mediator source which apply a simple transform mapping to it's source value
  void addTransformSource<X>(LiveData<X> source, Y Function(X) transform) =>
      addSource<X>(source, (value) => postData(transform(value)));

  /// Add mediator source which uses the transformed live data as mediator source
  void addLiveTransformSource<X>(LiveData<X> source, LiveData<Y> Function(X) transform) {
    LiveData<Y>? lastSource;
    addSource<X>(source, (value) {
      final newLiveData = transform(value);
      var source = lastSource;
      if (identical(source, newLiveData)) return;
      if (source != null) removeSource(source);
      source = newLiveData;
      addSource(source, postData);
      lastSource = source;
    });
  }

  /// Add the given [source] to the mediator sources list
  void addSource<X>(LiveData<X> source, Observer<X> onChanged) {
    final existing = _sources[source];
    if (existing != null) {
      if (existing.observer != onChanged)
        throw ArgumentError('This source was already added with the different observer');
      return;
    }
    final sourceWrapper = _Source<X>(source, onChanged);
    _sources[source] = sourceWrapper;
    if (isActive) sourceWrapper.plug();
  }

  /// Removes the given [source] from the mediator sources list.
  void removeSource<X>(LiveData<X> source) => _sources.remove(source)?.unplug();
}

/// Used to wrap sources of a [MediatorMixin] LiveData
class _Source<X> {
  final LiveData<X> liveData;
  final Observer<X> observer;
  int _version = LiveData._uninitialized;

  _Source(this.liveData, this.observer);

  void plug() => liveData.observeForever(_onChanged);

  void unplug() => liveData.removeObserver(_onChanged);

  void _onChanged(X value) {
    if (_version < liveData._version) {
      _version = liveData._version;
      observer(value);
    }
  }
}

extension MediatorExt<Y> on MediatorMixin<Y> {
  /// Add a pair of sources to a [MediatorMixin] which will be transformed into a single value
  void addTransformSourcePair<X1, X2>(LiveData<X1> source1, LiveData<X2> source2, Y Function(X1, X2) transform) {
    addTransformSource<X1>(source1, (value) => transform(value, source2._data));
    addTransformSource<X2>(source2, (value) => transform(source1._data, value));
  }

  /// Add a triad of sources to a [MediatorMixin] which will be transformed into a single value
  void addTransformSourceTriple<X1, X2, X3>(
      LiveData<X1> source1, LiveData<X2> source2, LiveData<X3> source3, Y Function(X1, X2, X3) transform) {
    addTransformSource<X1>(source1, (value) => transform(value, source2._data, source3._data));
    addTransformSource<X2>(source2, (value) => transform(source1._data, value, source3._data));
    addTransformSource<X3>(source3, (value) => transform(source1._data, source2._data, value));
  }
}
