part of '../flutterx_live_data.dart';

/// [LiveData] mixin which keeps track of each modification to it's value and allows to undo or redo changes
///
/// Note: in order to use this LiveData, the value must be serialized and deserialized.
/// This is done by [jsonEncode] and [jsonEncode] by default but you can implement you custom deserialization logic
/// by overriding [serialize] and [deserialize] methods
///
/// To know you can undo/redo the history you can query [canUndo] and [canRedo] value
/// Once you are sure you can execute a certain action you can call [historyUndo] and [historyRedo] methods to
/// undo/redo a change made to this LiveData, or you can call anytime [historyClear] to completely clear the history
///
/// You also can set [maxStates] field which determines the max size of the history (by default is 20)
/// If you set a length that is less than the current history queue the first elements of the history will be
/// removed until the history length matches the desired value
///
/// [T] The type of data held by this instance
mixin HistoryMixin<T> on LiveData<T> {
  final Queue<String> _queue = Queue();
  int _maxStates = 20;
  int _currentIndex = -1;
  bool _restoring = false;

  /// Tells if you can safely call [historyUndo].
  final LiveData<bool> canUndo = MutableLiveData(initialValue: false);

  /// Tells if you can safely call [historyRedo].
  final LiveData<bool> canRedo = MutableLiveData(initialValue: false);

  @override
  void setData(T value) {
    super.setData(value);
    _restoring = false;
  }

  @override
  void invalidate() {
    super.invalidate();
    if (!_restoring) _saveState();
  }

  /// Get max length of history queue
  int get maxStates => _maxStates;

  /// Set max length of history queue and automatically resize history to match desired length
  set maxStates(int value) {
    _maxStates = value;
    while (_queue.length >= value) _queue.removeFirst();
  }

  /// Get current index of the history
  int get historyIndex => _currentIndex;

  /// Get current history queue length
  int get historyLength => _queue.length;

  /// Reset current LiveData value to previous dispatched value
  void historyUndo() {
    assert(canUndo.value);
    _currentIndex--;
    _restoreState();
    _updateValues();
  }

  /// Reset current LiveData value to next dispatched value
  void historyRedo() {
    assert(canRedo.value);
    _currentIndex++;
    _restoreState();
    _updateValues();
  }

  /// Clear history queue. This will not alter current LiveData value
  void historyClear() {
    _queue.clear();
    _currentIndex = -1;
    _updateValues();
  }

  /// Convert given value to string
  @protected
  String serialize(T data) => jsonEncode(data);

  /// Convert given string to value
  @protected
  T deserialize(String data) => jsonDecode(data);

  /// Save current state to history queue
  void _saveState() {
    if (uninitialized) return;
    final state = serialize(_data);
    while (_queue.length > _currentIndex + 1) _queue.removeLast();
    if (_queue.isEmpty || state != _queue.last) {
      _queue.add(state);
      _currentIndex == _maxStates - 1 ? _queue.removeFirst() : _currentIndex++;
      _updateValues();
    }
  }

  /// Restore a previously saved state from history queue
  void _restoreState() {
    _restoring = true;
    postData(deserialize(_queue.elementAt(_currentIndex)));
  }

  /// Keep [canUndo] and [canRedo] updated
  void _updateValues() {
    (canUndo as MutableLiveData).value = _currentIndex > 0;
    (canRedo as MutableLiveData).value = _currentIndex < _queue.length - 1;
  }
}
