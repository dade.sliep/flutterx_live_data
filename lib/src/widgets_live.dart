part of '../flutterx_live_data.dart';

/// Builds a [Widget] when a concrete value of [LiveData] is observed.
typedef LiveWidgetBuilder<T> = Widget Function(BuildContext context, T value);

/// A [Widget] whose content is built from the observed [data]
///
/// [T] The type of data held by [data]
class LiveDataBuilder<T> extends StatelessWidget {
  /// LiveData observed by this widget
  final LiveData<T> data;

  /// [LiveWidgetBuilder] of this widget content
  final LiveWidgetBuilder<T> builder;

  /// Placeholder to show instead of [builder] when [LiveData.initialized] is false. Defaults to [SizedBox.shrink]
  final Widget? placeholder;

  /// Create a [LiveDataBuilder] for [data]
  const LiveDataBuilder({
    Key? key,
    required this.data,
    required this.builder,
    this.placeholder,
  }) : super(key: key);

  /// [LiveDataBuilder] are controlled by [LiveDataBuilderElement]
  @override
  LiveDataBuilderElement<T> createElement() => LiveDataBuilderElement<T>(this);

  /// Delegates to [liveBuild]
  @override
  Widget build(BuildContext context) => liveBuild<T>(context, data: data, builder: builder, placeholder: placeholder);
}

/// Build widget with the live value of [data]
Widget liveBuild<T>(
  BuildContext context, {
  required LiveData<T> data,
  required LiveWidgetBuilder<T> builder,
  Widget? placeholder,
}) =>
    data.initialized && data.isActive ? builder(context, data._data) : (placeholder ?? const SizedBox.shrink());

/// Element of [LiveDataBuilder]
class LiveDataBuilderElement<T> extends StatelessElement {
  @override
  LiveDataBuilder<T> get widget => super.widget as LiveDataBuilder<T>;

  LiveDataBuilderElement(LiveDataBuilder<T> widget) : super(widget);

  @override
  void mount(Element? parent, dynamic newSlot) {
    super.mount(parent, newSlot);
    link(widget.data);
  }

  @override
  void update(covariant StatelessWidget newWidget) {
    final oldData = widget.data;
    super.update(newWidget);
    if (!identical(oldData, widget.data)) {
      unlink(oldData);
      link(widget.data);
    }
  }

  @override
  void unmount() {
    unlink(widget.data);
    super.unmount();
  }

  /// Bind this element to [data]
  void link(LiveData<T> data) => data.observeForever(onChanged);

  /// Unbind this element from [data]
  void unlink(LiveData<T> data) => data.removeObserver(onChanged);

  /// Handle [LiveDataBuilder.data] updates
  void onChanged(T value) => markNeedsBuild();
}
