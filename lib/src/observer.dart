part of '../flutterx_live_data.dart';

/// A simple callback that can receive from [LiveData].
typedef Observer<T> = void Function(T value);

/// Wrapper for [Observer]. This should be passed to [LiveData.addObserver] from a subclass of it
class ObserverWrapper<T> {
  /// Live data which owns this observer
  final LiveData<T> data;

  /// Called when the data is changed
  final Observer<T> onChanged;

  /// Active state of this observer. Should be set only by [LiveData] due to [activeStateChanged] call
  bool active = false;

  /// Version to be compared to live data's version
  int lastVersion = -1;

  /// Creates a [ObserverWrapper] (it should be used by a [LiveData] subclass only)
  ObserverWrapper(this.data, this.onChanged);

  /// The condition which influences active state of this observer
  bool get shouldBeActive => true;

  /// Notify live data that the active state of this observer has changed
  void activeStateChanged({required bool active}) => data._onActiveStateChanged(this, active: active);

  /// Called when the [Observer] is added to the [LiveData] observers list
  void onAdded() => activeStateChanged(active: shouldBeActive);

  /// Called when the [Observer] is removed from the [LiveData] observers list
  void onRemove() => activeStateChanged(active: false);
}
