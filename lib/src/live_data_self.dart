part of '../flutterx_live_data.dart';

/// [LiveData] whose [value] reflects itself
/// This means that the [value] of this LiveData is always this instance
/// Useful if you want to create a LiveData singleton object that notifies observers every time [invalidate] is called
/// By default [setData] or [postData] do nothing, but you can override this methods to manually update this instance
/// and call [postInvalidate], anyway you can never update [value]
///
/// [T] The type of the subclass that implements SelfLiveData
abstract class SelfLiveData<T extends SelfLiveData<T>> extends LiveData<T> {
  /// Creates a [SelfLiveData] initialized with this.
  SelfLiveData({Dispatcher dispatcher = Dispatcher.postFrame}) : super(dispatcher: dispatcher) {
    super.setData(this as T);
  }

  /// this != this is always false
  @override
  @nonVirtual
  bool shouldUpdateValue(T oldValue, T newValue) => false;

  /// Cannot set this instance
  @override
  void postData(T value) => throw UnsupportedError('cannot set this instance');

  /// Cannot set this instance
  @override
  void setData(T value) => throw UnsupportedError('cannot set this instance');
}
