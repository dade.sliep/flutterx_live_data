part of '../flutterx_live_data.dart';

/// LiveData is a data holder class that can be observed.
/// This is a Flutter implementation of LiveData in Android Jetpack library
///
/// A LiveData has a value which by default is uninitialized.
/// If the current value of this LiveData has been initialized, it will be dispatched to every observer
/// that becomes active
///
/// An [Observer] can be added and will be notified about modifications of the wrapped data only
/// if is in active state.
/// An observer added via [observeForever] is considered as always active and thus will be always notified
/// about modifications. For those observers, you should manually call [removeObserver]
///
/// To observe a LiveData inside a [State] you should implement [StateObserver] mixin on that state,
/// override [StateObserver.registerObservers] and here you can call [StateObserver.observe]
///
/// Alternatively You can use [LiveDataBuilder] and [MediatorLiveDataBuilder] to handle live data changes
/// withing the scope of a widget tree. The widget is automatically rebuilt every time the data is updated
///
/// Each modification can happen through protected [setData] and [postData] methods
/// The first one sets the data immediately and [invalidate] this live data,
/// while [postData] posts a task to call [setData] to the relative [dispatcher]
///
/// [setData] will update the current value and invalidates this LiveData only if [shouldUpdateValue] returns true
/// This check changes from one implementation to another, but by default is newValue != oldValue
/// This means that if you set the same value twice consecutively only the first time it will be dispatched to the
/// observers
///
/// In addition, LiveData has [onActive] and [onInactive] methods
/// to get notified when number of active [Observer]s change between 0 and 1.
/// This allows LiveData to release any heavy resources when it does not have any Observers that
/// are actively observing.
///
/// [T] The type of data held by this instance
abstract class LiveData<T> {
  static const int _uninitialized = -1;

  /// [Dispatcher] of this LiveData used in [postData]
  final Dispatcher dispatcher;
  final Map<Observer<T>, ObserverWrapper<T>> _observers = {};
  late T _data;
  int _version = _uninitialized;
  int _activeCount = 0;

  late T _pendingData;
  bool _dispatchingPendingData = false;
  bool _dispatching = false;
  bool _dispatchInvalidated = false;

  /// Creates a uninitialized LiveData with the given [Dispatcher]
  LiveData({required this.dispatcher});

  /// Version of this LiveData.
  /// Starts from [_uninitialized] and is incremented every time [invalidate] method is called
  int get version => _version;

  /// [returns] true if this LiveData has active observers.
  bool get isActive => _activeCount > 0;

  /// [returns] if the data has been initialized. Negative of [uninitialized]
  bool get initialized => _version > _uninitialized;

  /// [returns] if the data has not been initialized. It means that accessing data in any way will throw
  /// uninitialized exception
  bool get uninitialized => _version == _uninitialized;

  /// [returns] the current value.
  /// Note that this getter returns the latest value set, it's not guaranteed that corresponds to latest value
  /// dispatched
  T get value => _dispatchingPendingData ? _pendingData : _data;

  /// Called when the number of active observers change from 0 to 1 and [isActive] becomes true.
  ///
  /// This callback can be used to know that this LiveData is being used thus should be kept
  /// up to date.
  @protected
  void onActive() {}

  /// Called when the number of active observers change from 1 to 0and [isActive] becomes false.
  ///
  /// This does not mean that there are no observers left, there may still be observers but their
  /// state is not active.
  @protected
  void onInactive() {}

  /// [returns] true if [newValue] should be set by [setData] and dispatched,
  /// or false to keep [oldValue] and do nothing
  @protected
  bool shouldUpdateValue(T oldValue, T newValue) => newValue != oldValue;

  /// Sets the [value]. If there are active observers, the value will be dispatched to them.
  ///
  /// This method should only be called by subclasses of LiveData depending on the implementation.
  /// To directly update the value of a [MutableLiveData] you can use [MutableLiveData.value] setter
  ///
  /// This method will set and dispatch the value synchronously to it's call
  /// To dispatch data to the [dispatcher] call [postData]
  @protected
  void setData(T value) {
    if (uninitialized || shouldUpdateValue(_data, value)) {
      _data = value;
      invalidate();
    }
  }

  /// Post value implementation (this should only be called by subclasses of LiveData)
  /// Implicitly calls [setData] after [_postFutureFactory]'s future is completed if not null
  /// Posts a task to [dispatcher] to set the given value.
  /// Depending on the dispatcher assigned to this LiveData if you have a following code executed during build phase:
  /// ```dart
  /// liveData.postData("a");
  /// liveData.setData("b");
  /// ```
  /// And for e.g. the dispatcher is [Dispatcher.postFrame], the value "b" would be set at first and later
  /// the postFrame would override it with the value "a".
  /// Instead if the dispatcher is [Dispatcher.immediate] the value "a" will be immediately set and then overwritten
  /// by value "b"
  ///
  /// If you called this method multiple times before a postFrame executed a posted task, only
  /// the last value would be dispatched.
  @protected
  void postData(T value) {
    _pendingData = value;
    if (_dispatchingPendingData) return;
    dispatcher.post(() => setData(_pendingData), (value) => _dispatchingPendingData = value);
  }

  /// Adds the given observer to the observers list.
  /// Similar to [addObserver] wrapping onChanged with default [ObserverWrapper] implementation
  /// DO NOT call this method more than once with the same [onChanged]
  ///
  /// Once an observer is added through this method is always active.
  /// This means that the given observer will receive all events and will never
  /// be automatically removed. You should manually call [removeObserver] to stop
  /// observing this LiveData.
  ///
  /// [onChanged] is the observer that will receive the events
  void observeForever(Observer<T> onChanged) => addObserver(ObserverWrapper<T>(this, onChanged));

  /// Adds the given [ObserverWrapper] implementation to the observers list.
  ///
  /// DO NOT call this method out of LiveData subclasses. Instead extend LiveData and create
  /// an observe...(...) method that wraps [Observer] with a custom implementation of [ObserverWrapper]
  /// DO NOT call this method more than once with the same [ObserverWrapper.onChanged]
  /// While LiveData has one of such observers, it will be considered as active.
  /// This will immediately trigger [ObserverWrapper.onAdded] function of observer
  ///
  /// [observer] is the wrapper of the observer that will receive the events
  @protected
  void addObserver(ObserverWrapper<T> observer) {
    assert(!_observers.containsKey(observer.onChanged), 'observer already registered');
    _observers[observer.onChanged] = observer;
    observer.onAdded();
  }

  /// Removes the given [observer] from the observers list.
  void removeObserver(Observer<T> observer) => _observers.remove(observer)?.onRemove();

  /// Increment version and dispatch current value no matter if is changed
  /// Usually this method should only be called from [setData]
  /// It can be also useful if you want to notify observer that even though data remained the same,
  /// one of it's field changes (in that case also consider make it an
  /// instance of LiveData or use [LiveCollection] for collections)
  /// Consider using [postInvalidate]
  void invalidate() {
    _version++;
    _dispatchingValue(null);
  }

  /// Post [invalidate] call to [dispatcher]
  void postInvalidate() => dispatcher.post(invalidate);

  /// Called by [ObserverWrapper.activeStateChanged] to notify active state of [observer] is changed
  void _onActiveStateChanged(ObserverWrapper<T> observer, {required bool active}) {
    if (observer.active == active) return;
    final wasActive = isActive;
    observer.active = active;
    _activeCount += active ? 1 : -1;
    if (wasActive != isActive) wasActive ? onInactive() : onActive();
    if (active) _dispatchingValue(observer);
  }

  /// Dispatch live data value to observers list
  /// If [initiator] is null it means this function has been triggered by [invalidate],
  /// else initiator is the observer that has changed his state and the event must be dispatched only to it
  void _dispatchingValue(ObserverWrapper<T>? initiator) {
    if (uninitialized) return;
    if (_dispatching) {
      _dispatchInvalidated = true;
      return;
    }
    _dispatching = true;
    do {
      _dispatchInvalidated = false;
      if (initiator != null) {
        _considerNotify(initiator);
        initiator = null;
      } else {
        for (final observer in _observers.values.toList(growable: false)) {
          _considerNotify(observer);
          if (_dispatchInvalidated) break;
        }
      }
    } while (_dispatchInvalidated);
    _dispatching = false;
  }

  /// Notify data change to observer (if observer is active and live data version is greater that observer one)
  void _considerNotify(ObserverWrapper<T> observer) {
    if (!observer.active) return;
    if (!observer.shouldBeActive) return _onActiveStateChanged(observer, active: false);
    if (_version > observer.lastVersion) {
      observer.lastVersion = _version;
      observer.onChanged(_data);
    }
  }

  /// Describes this LiveData and value
  @override
  String toString() => '$runtimeType(${uninitialized ? '<uninitialized>' : identical(value, this) ? 'this' : value})';
}

/// MediatorLiveData is the implementation of [MediatorMixin] for [LiveData]
class MediatorLiveData<T> extends LiveData<T> with MediatorMixin<T> {
  /// [dispatcher] by default is [Dispatcher.immediate]
  MediatorLiveData({Dispatcher dispatcher = Dispatcher.immediate}) : super(dispatcher: dispatcher);
}
