part of '../flutterx_live_data.dart';

/// [LiveData] which publicly exposes [value] setter which [postData] to [dispatcher]
///
/// [T] The type of data held by this instance
class MutableLiveData<T> extends LiveData<T> {
  /// Creates a uninitialized [MutableLiveData]
  /// This means that if you try to call [value] getter before data has been set,
  /// a [LateInitializationError] will be thrown
  /// [dispatcher] by default is [Dispatcher.postFrame]
  MutableLiveData.late({Dispatcher dispatcher = Dispatcher.postFrame}) : super(dispatcher: dispatcher);

  /// Creates a [MutableLiveData] initialized with the given [initialValue].
  /// [dispatcher] by default is [Dispatcher.postFrame]
  MutableLiveData({required T initialValue, Dispatcher dispatcher = Dispatcher.postFrame})
      : super(dispatcher: dispatcher) {
    setData(initialValue);
  }

  /// Set LiveData current value to [value]
  /// The value may not be dispatched immediately depending on the [dispatcher] of this LiveData
  set value(T value) => postData(value);
}

/// MutableMediatorLiveData is the implementation of [MediatorMixin] for [MutableLiveData]
class MutableMediatorLiveData<T> extends MutableLiveData<T> with MediatorMixin<T> {
  /// check [MutableLiveData.late] constructor
  MutableMediatorLiveData.late({Dispatcher dispatcher = Dispatcher.postFrame}) : super.late(dispatcher: dispatcher);

  /// check [MutableLiveData] constructor
  MutableMediatorLiveData({required T initialValue, Dispatcher dispatcher = Dispatcher.postFrame})
      : super(initialValue: initialValue, dispatcher: dispatcher);
}

/// HistoryMutableLiveData is the implementation of [HistoryMixin] for [MutableLiveData]
class HistoryMutableLiveData<T> extends MutableLiveData<T> with HistoryMixin<T> {
  /// check [MutableLiveData.late] constructor
  HistoryMutableLiveData.late({Dispatcher dispatcher = Dispatcher.postFrame}) : super.late(dispatcher: dispatcher);

  /// check [MutableLiveData] constructor
  HistoryMutableLiveData({required T initialValue, Dispatcher dispatcher = Dispatcher.postFrame})
      : super(initialValue: initialValue, dispatcher: dispatcher);
}
