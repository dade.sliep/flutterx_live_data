part of '../flutterx_live_data.dart';

/// [LiveData] which dispatch it's value only when [invalidate] is called
/// By default a LiveData dispatch it's current data to newly activated observers, LiveEvent instead notifies
/// it's observers only once when you call [send].
/// If an [Observer] it's added after an event is sent it won't be notified of that event, anyway it can be
/// notified of the following events sent as long as it remains active.
///
/// Contrary to LiveData, if you call [send] with the same value twice consecutively it will be dispatched every time
///
/// [T] The type of event this instance can send
class LiveEvent<T> extends LiveData<T> {
  /// Creates a [LiveEvent]
  /// [dispatcher] by default is [Dispatcher.postFrame]
  LiveEvent({Dispatcher dispatcher = Dispatcher.postFrame}) : super(dispatcher: dispatcher);

  /// Dispatch the event to to the observers of this LiveEvent
  void send(T value) => postData(value);

  /// Every event can happen multiple times
  @override
  bool shouldUpdateValue(T oldValue, T newValue) => true;

  /// Dispatch only global events triggered by [invalidate]
  @override
  void _dispatchingValue(ObserverWrapper<T>? initiator) => initiator == null ? super._dispatchingValue(null) : null;
}

/// MediatorLiveEvent is the implementation of [MediatorMixin] for [LiveEvent]
class MediatorLiveEvent<T> extends LiveEvent<T> with MediatorMixin<T> {
  /// check [LiveEvent] constructor
  MediatorLiveEvent({Dispatcher dispatcher = Dispatcher.immediate}) : super(dispatcher: dispatcher);
}
