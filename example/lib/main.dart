import 'package:flutter/material.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutterx LiveData Demo',
      theme: ThemeData(primarySwatch: Colors.teal),
      home: const LiveDataExample());
}

class LiveDataExample extends StatefulWidget {
  const LiveDataExample({Key? key}) : super(key: key);

  @override
  State<LiveDataExample> createState() => _LiveDataExampleState();
}

class _LiveDataExampleState extends State<LiveDataExample> {
  final MutableLiveData<int> myCounter = MutableLiveData(initialValue: 0);
  final MutableLiveData<int> myCounter2 = MutableLiveData(initialValue: 0);

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: const Text('LiveData example')),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(onPressed: () => myCounter2.value++, child: const Text('PRESS')),
          LiveDataBuilder<int>(data: myCounter, builder: (context, value) => Text('pressed $value times')),
          LiveDataBuilder<int>(data: myCounter2, builder: (context, value) => Text('pressed2 $value times')),
          CombinedLiveDataBuilder<int, int, int>(
              data1: myCounter,
              data2: myCounter2,
              transform: (a, b) => a + b,
              builder: (context, value) => Text('total: pressed $value times')),
        ],
      )),
      floatingActionButton: FloatingActionButton(onPressed: () => myCounter.value++, child: const Icon(Icons.add)));
}
