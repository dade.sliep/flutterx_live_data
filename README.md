# flutterx_live_data

LiveData is a data holder class that can be observed. This is a Flutter implementation of LiveData in Android Jetpack library.

## Import

Import the library like this:

```dart
import 'package:flutterx_live_data/flutterx_live_data.dart';
```

## Usage

Check the documentation in the desired source file of this library